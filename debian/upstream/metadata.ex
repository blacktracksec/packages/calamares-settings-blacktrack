# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/calamares-settings-blacktrack/issues
# Bug-Submit: https://github.com/<user>/calamares-settings-blacktrack/issues/new
# Changelog: https://github.com/<user>/calamares-settings-blacktrack/blob/master/CHANGES
# Documentation: https://github.com/<user>/calamares-settings-blacktrack/wiki
# Repository-Browse: https://github.com/<user>/calamares-settings-blacktrack
# Repository: https://github.com/<user>/calamares-settings-blacktrack.git
